package ru.dmitriyharin.football;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FootballService {
    public static void main(String[] args) {
        SpringApplication.run(FootballService.class, args);
    }
}